using Discord.Commands;
using System.Threading.Tasks;
using System.Diagnostics;
using RanBot.Common.Attributes;

namespace RanBot.Modules
{
    public partial class SelfModule : ModuleBase<SocketCommandContext>
    {
        [RanCommand, Aliases, Use, Description]
        public async Task Ping()
        {
            var sw = Stopwatch.StartNew();
            var msg = await ReplyAsync("🏓").ConfigureAwait(false);
            sw.Stop();

            await msg.DeleteAsync();

            await ReplyAsync($"🏓 {(int)sw.Elapsed.TotalMilliseconds}ms");
        }
    }
}