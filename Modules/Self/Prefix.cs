using Discord;
using Discord.Commands;
using Discord.WebSocket;
using System.Threading.Tasks;
using RanBot.Common.Attributes;
using RanBot.Extensions;
using RanBot.Services;
using RanBot.Services.Impl;

namespace RanBot.Modules
{
    public partial class SelfModule : ModuleBase<SocketCommandContext>
    {
        private static readonly IBotCredentials _creds = new BotCredentials();
        private static readonly DbService _db = new DbService(_creds);
        [RanCommand, Aliases, Use, Description]
        [Priority (0)]
        public async Task Prefix()
        {
            var currentPrefix = Context.Message.GetPrefix();
            var embed = new EmbedBuilder()
                .WithDescription($"Current prefix is {currentPrefix}")
                .WithOkColor(Context.Message);
            await Context.Channel.SendMessageAsync("", embed: embed.Build());
        }

        [RanCommand, Aliases, Use, Description]
        [RequireUserPermission (GuildPermission.Administrator)]
        [Priority (1)]
        public async Task Prefix(string prefix)
        {
            using(var uow = _db.GetDbContext())
            {
                var channel = Context.Message.Channel as SocketTextChannel;
                var config = uow.GuildConfigs.ForId(channel.Guild.Id);
                config.Prefix = prefix;
                uow.SaveChanges();
            }
            var embed = new EmbedBuilder()
                .WithDescription($"You got it. Prefix set to {prefix}")
                .WithOkColor(Context.Message);
            await Context.Channel.SendMessageAsync("", embed: embed.Build());
        }

        [RanCommand, Aliases, Use, Description]
        [RequireOwner]
        public async Task DefPrefix(string prefix)
        {
            using(var uow = _db.GetDbContext())
            {
                var bc = uow.BotConfig.GetOrCreate();
                bc.DefaultPrefix = prefix;
                uow.SaveChanges();
            }
            var embed = new EmbedBuilder()
                .WithDescription($"Default prefix changed to {prefix}")
                .WithOkColor(Context.Message);
            await Context.Channel.SendMessageAsync("", embed: embed.Build());
        }
    }
}