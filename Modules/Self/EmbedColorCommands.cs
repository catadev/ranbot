using Discord;
using Discord.Commands;
using Discord.WebSocket;
using System.Threading.Tasks;
using RanBot.Common.Attributes;
using RanBot.Extensions;

namespace RanBot.Modules
{
    public partial class SelfModule : ModuleBase<SocketCommandContext>
    {
        [RanCommand, Aliases, Use, Description]
        [RequireUserPermission (GuildPermission.Administrator)]
        public async Task OkColor(string color)
        {
            if (color.ValidateHexColor())
            {
                using (var uow = _db.GetDbContext())
                {
                    var channel = Context.Message.Channel as SocketTextChannel;
                    var config = uow.GuildConfigs.ForId(channel.Guild.Id);
                    config.OkColor = color;
                    uow.SaveChanges();
                }
                var embed = new EmbedBuilder()
                    .WithDescription($"You got it. OkColor set to {color}")
                    .WithOkColor(Context.Message);
                await Context.Channel.SendMessageAsync("", embed: embed.Build());
            }
            else
            {
                var embed = new EmbedBuilder()
                    .WithDescription($"Please enter a valid color hex code.")
                    .WithErrorColor(Context.Message);
                await Context.Channel.SendMessageAsync("", embed: embed.Build());   
            }
        }

        [RanCommand, Aliases, Use, Description]
        [RequireUserPermission (GuildPermission.Administrator)]
        public async Task ErrorColor(string color)
        {
            if (color.ValidateHexColor())
            {
                using (var uow = _db.GetDbContext())
                {
                    var channel = Context.Message.Channel as SocketTextChannel;
                    var config = uow.GuildConfigs.ForId(channel.Guild.Id);
                    config.ErrorColor = color;
                    uow.SaveChanges();
                }
                var embed = new EmbedBuilder()
                    .WithDescription($"You got it. Error Color set to {color}")
                    .WithOkColor(Context.Message);
                await Context.Channel.SendMessageAsync("", embed: embed.Build());
            }
            else
            {
                var embed = new EmbedBuilder()
                    .WithDescription($"Please enter a valid color hex code.")
                    .WithErrorColor(Context.Message);
                await Context.Channel.SendMessageAsync("", embed: embed.Build());   
            }
        }

        [RanCommand, Aliases, Use, Description]
        [RequireOwner]
        public async Task DefOkColor(string color)
        {
            if (color.ValidateHexColor())
            {
                using (var uow = _db.GetDbContext())
                {
                    var bc = uow.BotConfig.GetOrCreate();
                    bc.OkColor = color;
                    uow.SaveChanges();
                }
                var embed = new EmbedBuilder()
                    .WithDescription($"Default OkColor changed to {color}")
                    .WithOkColor(Context.Message);
                await Context.Channel.SendMessageAsync("", embed: embed.Build());
            }
            else
            {
                var embed = new EmbedBuilder()
                    .WithDescription($"Please enter a valid color hex code.")
                    .WithErrorColor(Context.Message);
                await Context.Channel.SendMessageAsync("", embed: embed.Build());   
            }
        }

        [RanCommand, Aliases, Use, Description]
        [RequireOwner]
        public async Task DefErrorColor(string color)
        {
            if (color.ValidateHexColor())
            {
                using (var uow = _db.GetDbContext())
                {
                    var bc = uow.BotConfig.GetOrCreate();
                    bc.ErrorColor = color;
                    uow.SaveChanges();
                }
                var embed = new EmbedBuilder()
                    .WithDescription($"Default ErrorColor changed to {color}")
                    .WithOkColor(Context.Message);
                await Context.Channel.SendMessageAsync("", embed: embed.Build());
            }
            else
            {
                var embed = new EmbedBuilder()
                    .WithDescription($"Please enter a valid color hex code.")
                    .WithErrorColor(Context.Message);
                await Context.Channel.SendMessageAsync("", embed: embed.Build());   
            }
        }
    }
}