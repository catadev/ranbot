using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Discord;
using Discord.WebSocket;
using Discord.Commands;


namespace RanBot.Services
{
    public class RanLogger
    {
        private readonly DiscordSocketClient _client;
        private readonly CommandService _commands;
        private readonly ILoggerFactory _loggerFactory;
        private readonly ILogger _clientLogger;
        private readonly ILogger _commandsLogger;

        public RanLogger(DiscordSocketClient client, CommandService commands, ILoggerFactory loggerFactory)
        {
            _client = client;
            _commands = commands;

            _loggerFactory = ConfigureLogging(loggerFactory);
            _clientLogger = _loggerFactory.CreateLogger("discord");
            _commandsLogger = _loggerFactory.CreateLogger("commands");

            _client.Log += LogClient;
            _commands.Log += LogCommand;
        }

         private ILoggerFactory ConfigureLogging(ILoggerFactory factory)
        {
            factory = LoggerFactory.Create( builder => builder.AddConsole());
            return factory;
        }

        private Task LogClient(LogMessage message)
        {
            _clientLogger.Log(
                LogLevelFromSeverity(message.Severity), 
                0, 
                message,
                message.Exception, 
                (_1, _2) => message.ToString(prependTimestamp: false));
            return Task.CompletedTask;
        }

        private Task LogCommand(LogMessage message)
        {
            _commandsLogger.Log(
                LogLevelFromSeverity(message.Severity),
                0,
                message,
                message.Exception,
                (_1, _2) => message.ToString(prependTimestamp: false));
            return Task.CompletedTask;
        }

        private static LogLevel LogLevelFromSeverity(LogSeverity severity)
            => (LogLevel)(Math.Abs((int)severity - 5));
    }
}