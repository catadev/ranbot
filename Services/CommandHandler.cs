using Discord.WebSocket;
using RanBot.Extensions;
using Discord.Commands;
using System.Reflection;
using System.Threading.Tasks;
using System;

namespace RanBot.Services
{
    public class CommandHandler
    {
        private readonly DiscordSocketClient _client;
        private readonly CommandService _commands;
        private readonly IServiceProvider _provider;
        private readonly DbService _db;

        public CommandHandler(DiscordSocketClient client, CommandService commands, IServiceProvider provider, DbService db)
        {
            _client = client;
            _commands = commands;
            _provider = provider;
            _db = db;
            _client.MessageReceived += HandleCommandAsync;
        }

        private async Task InstallCommandsAsync(IServiceProvider provider, SocketCommandContext context)
        {
            using (var uow = _db.GetDbContext())
            {
                var channel = context.Channel as SocketTextChannel;
                if (channel != null)
                {
                    Console.WriteLine($"Loaded commands for channel{channel.Name}");
                }
            }
            await _commands.AddModulesAsync(assembly: Assembly.GetEntryAssembly(),
                                            services: null);
        }

        private async Task HandleCommandAsync(SocketMessage messageParam)
        {
            var message = messageParam as SocketUserMessage;
            if (message == null || message.Author.IsBot)
                return;

            int argPos = 0;

            var prefix = message.GetPrefix();
            if (!message.HasStringPrefix(prefix, ref argPos) && !message.HasMentionPrefix(_client.CurrentUser, ref argPos))
                return;

            var context = new SocketCommandContext(_client, message);

            await InstallCommandsAsync(_provider, context);

            await _commands.ExecuteAsync(
                context: context,
                argPos: argPos,
                services: _provider);
        }
    }
}