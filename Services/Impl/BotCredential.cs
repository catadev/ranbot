using System.IO;
using Microsoft.Extensions.Configuration;
using RanBot.Services;

namespace RanBot.Services.Impl
{
    public class BotCredentials : IBotCredentials
    {
        public string Token { get; }
        public DbConfig Db { get; }

        private readonly string _credsFileName = Path.Combine(Directory.GetCurrentDirectory(), "data/credentials.json");

        public BotCredentials()
        {
            var configBuilder = new ConfigurationBuilder();
                configBuilder.AddJsonFile(_credsFileName, true);
            var data = configBuilder.Build();

                Token = data[nameof(Token)];
                var dbSection = data.GetSection("db");
                Db = new DbConfig(string.IsNullOrWhiteSpace(dbSection["Type"])
                                ? "sqlite"
                                : dbSection["Type"],
                            string.IsNullOrWhiteSpace(dbSection["ConnectionString"])
                                ? "Data Source=data/RanBot.db"
                                : dbSection["ConnectionString"]);

        }

    }

}