using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using RanBot.Common;

namespace RanBot.Services.Impl
{
    public class CommandList
    {
        private readonly Dictionary<string, CommandData> _commandData;

        public CommandList(string locale)
        {
            _commandData = JsonConvert.DeserializeObject<Dictionary<string, CommandData>>(File.ReadAllText($"./strings/cmds/cmds_{locale}.json"));
        }
        public CommandData LoadCommand(string key)
        {
            this._commandData.TryGetValue(key, out var toReturn);

            if (toReturn == null)
                return new CommandData
                {
                    Cmd = key,
                    Aliases = key,
                    Desc = key,
                    Use = new[] { key },
                };

            return toReturn;
        }

    }

}