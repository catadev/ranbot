using Discord;
using System;
using System.Collections.Generic;

namespace RanBot.Services.Database.Models
{
    public class UserCountingStats : DbEntity
    {
        public ulong UserId { get; set; }
        public ulong GuildId { get; set; }
        public int Counted { get; set; }
    }
}