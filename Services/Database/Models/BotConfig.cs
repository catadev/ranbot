using System;
using System.Collections.Generic;

namespace RanBot.Services.Database.Models
{
    public class BotConfig : DbEntity
    {

        public string OkColor { get; set; } = "009113";
        public string ErrorColor { get; set; } = "910000";
        public string DefaultPrefix { get; set; } = "r!";
        public string DefaultLanguage { get; set; } = "en-US";
    }

}