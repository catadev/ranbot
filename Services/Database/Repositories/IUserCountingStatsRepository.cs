using RanBot.Services.Database.Models;
using System.Collections.Generic;

namespace RanBot.Services.Database.Repositories
{
    public interface IUserCountingStatsRepository : IRepository<UserCountingStats>
    {
        UserCountingStats GetOrCreateUser(ulong guildId, ulong userId);
        int GetUserGuildRanking(ulong userId, ulong guildId);
        List<UserCountingStats> GetUsersFor(ulong guildId, int page);
        void ResetGuildUserCounting(ulong userId, ulong guildId);
        void ResetGuildCounting(ulong guildId);
        List<UserCountingStats> GetTopUserCountings(ulong guildId, int count);
    }
}