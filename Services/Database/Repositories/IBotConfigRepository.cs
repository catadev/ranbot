using Microsoft.EntityFrameworkCore;
using RanBot.Services.Database.Models;
using System;
using System.Linq;

namespace RanBot.Services.Database.Repositories
{
    public interface IBotConfigRepository : IRepository<BotConfig>
    {
        BotConfig GetOrCreate(Func<DbSet<BotConfig>, IQueryable<BotConfig>> includes = null);
    }
}
