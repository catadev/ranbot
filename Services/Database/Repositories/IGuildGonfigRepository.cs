using Microsoft.EntityFrameworkCore;
using RanBot.Services.Database.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace RanBot.Services.Database.Repositories
{
    public interface IGuildConfigRepository : IRepository<GuildConfig>
    {
        GuildConfig ForId(ulong guildId, Func<DbSet<GuildConfig>, IQueryable<GuildConfig>> includes = null);
    }
}