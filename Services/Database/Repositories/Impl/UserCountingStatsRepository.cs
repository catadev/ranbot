using RanBot.Services.Database.Models;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace RanBot.Services.Database.Repositories.Impl
{
    public class UserCountingStatsRepository : Repository<UserCountingStats>, IUserCountingStatsRepository
    {
        public UserCountingStatsRepository(DbContext context) : base(context)
        {
        }

        public UserCountingStats GetOrCreateUser(ulong guildId, ulong userId)
        {
            var usr = _set.FirstOrDefault(x => x.UserId == userId && x.GuildId == guildId);

            if (usr == null)
            {
                _context.Add(usr = new UserCountingStats()
                {
                    Counted = 0,
                    UserId = userId,
                    GuildId = guildId,
                });
            }

            return usr;
        }

        public List<UserCountingStats> GetUsersFor(ulong guildId, int page)
        {
            return _set
                .AsQueryable()
                .Where(x => x.GuildId == guildId)
                .OrderByDescending(x => x.Counted)
                .Skip(page * 9)
                .Take(9)
                .ToList();
        }

        public List<UserCountingStats> GetTopUserCountings(ulong guildId, int count)
        {
            return _set
                .AsQueryable()
                .Where(x => x.GuildId == guildId)
                .OrderByDescending(x => x.Counted)
                .Take(count)
                .ToList();
        }

        public int GetUserGuildRanking(ulong userId, ulong guildId)
        {

            return _set
                .AsQueryable()
                .Where(x => x.GuildId == guildId && ((x.Counted) >
                    (_set.AsQueryable()
                        .Where(y => y.UserId == userId && y.GuildId == guildId)
                        .Select(y => y.Counted)
                        .FirstOrDefault())
                ))
                .Count() + 1;
        }

        public void ResetGuildUserCounting(ulong userId, ulong guildId)
        {
            _context.Database.ExecuteSqlInterpolated($"DELETE FROM UserCountingStats WHERE UserId={userId} AND GuildId={guildId};");
        }

        public void ResetGuildCounting(ulong guildId)
        {
            _context.Database.ExecuteSqlInterpolated($"DELETE FROM UserCountingStats WHERE GuildId={guildId};");
        }
    }
}
