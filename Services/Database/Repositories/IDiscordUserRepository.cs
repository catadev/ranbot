using Discord;
using RanBot.Services.Database.Models;
using System.Collections.Generic;

namespace RanBot.Services.Database.Repositories
{
    public interface IDiscordUserRepository : IRepository<DiscordUser>
    {
        void EnsureCreated(ulong userId, string username, string discrim);
        DiscordUser GetOrCreate(ulong userId, string username, string discrim);
        DiscordUser GetOrCreate(IUser original);
        DiscordUser[] GetUsersCountLeaderboardFor(int page);

    }
}
