using System;
using System.Threading.Tasks;
using RanBot.Services.Database.Repositories;
using RanBot.Services.Database.Repositories.Impl;

namespace RanBot.Services.Database
{
    public sealed class UnitOfWork : IUnitOfWork
    {
        public RanContext _context { get; }

        private IGuildConfigRepository _guildConfigs;
        public IGuildConfigRepository GuildConfigs => _guildConfigs ?? (_guildConfigs = new GuildConfigRepository(_context));

        private IBotConfigRepository _botConfig;
        public IBotConfigRepository BotConfig => _botConfig ?? (_botConfig = new BotConfigRepository(_context));

        private IDiscordUserRepository _discordUsers;
        public IDiscordUserRepository DiscordUsers => _discordUsers ?? (_discordUsers = new DiscordUserRepository(_context));

        private IUserCountingStatsRepository _userCountingStats;
        public IUserCountingStatsRepository UserCountingStats => _userCountingStats ?? (_userCountingStats = new UserCountingStatsRepository(_context));

        public UnitOfWork(RanContext context)
        {
            _context = context;
        }

        public int SaveChanges() =>
            _context.SaveChanges();

        public Task<int> SaveChangesAsync() =>
            _context.SaveChangesAsync();

        public void Dispose()
        {
            _context.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}
