namespace RanBot.Services
{
     public interface IBotCredentials
    {
        string Token { get; }
        DbConfig Db { get; }
    }


    public class DbConfig
    {
        public DbConfig(string type, string connectionString)
        {
            this.Type = type;
            this.ConnectionString = connectionString;
        }
        public string Type { get; }
        public string ConnectionString { get; }
    }    

}