using System;
using Discord;
using Discord.WebSocket;
using RanBot.Services;
using RanBot.Services.Impl;
using System.Globalization;

namespace RanBot.Extensions
{
    public static class Extensions
    {
        private static readonly IBotCredentials _creds = new BotCredentials();
        private static readonly DbService _db = new DbService(_creds);
        public static string GetPrefix(this SocketUserMessage message)
        {
            string prefix;
            using(var uow = _db.GetDbContext())
            {
                var channel = message.Channel as SocketTextChannel;
                var bc = uow.BotConfig.GetOrCreate();
                if(channel != null)
                {
                    var config = uow.GuildConfigs.ForId(channel.Guild.Id);
                    if(config.Prefix == null) 
                        prefix = bc.DefaultPrefix;
                    else
                        prefix = config.Prefix;
                }
                else
                    prefix = bc.DefaultPrefix;
            }
            return prefix;
        }

        public static EmbedBuilder WithOkColor(this EmbedBuilder eb, SocketUserMessage message)
        {
            using(var uow = _db.GetDbContext())
            {
                var channel = message.Channel as SocketTextChannel;
                var bc = uow.BotConfig.GetOrCreate();
                var defOkColor = new Color(Convert.ToUInt32(bc.OkColor, 16));
                if(channel != null)
                {
                    var config = uow.GuildConfigs.ForId(channel.Guild.Id);
                    if(config.OkColor == null) 
                        eb.WithColor(defOkColor);
                    else
                    {
                        var guildOkColor = new Color(Convert.ToUInt32(config.OkColor, 16));
                        eb.WithColor(guildOkColor);
                    }
                }
                else
                    eb.WithColor(defOkColor);
            }
            return eb;
        }

        public static EmbedBuilder WithErrorColor(this EmbedBuilder eb, SocketUserMessage message)
        {
            using(var uow = _db.GetDbContext())
            {
                var channel = message.Channel as SocketTextChannel;
                var bc = uow.BotConfig.GetOrCreate();
                var defErrorColor = new Color(Convert.ToUInt32(bc.ErrorColor, 16));
                if(channel != null)
                {
                    var config = uow.GuildConfigs.ForId(channel.Guild.Id);
                    if(config.ErrorColor == null) 
                        eb.WithColor(defErrorColor);
                    else
                    {
                        var guildErrorColor = new Color(Convert.ToUInt32(config.ErrorColor, 16));
                        eb.WithColor(guildErrorColor);
                    }
                }
                else
                    eb.WithColor(defErrorColor);
            }
            return eb;
        }

        public static bool ValidateHexColor(this string color)
        {
            int hexParseChecker;
            return (int.TryParse(color, NumberStyles.HexNumber, CultureInfo.InvariantCulture, out hexParseChecker)&& hexParseChecker > 16777215);
        }
    }
}